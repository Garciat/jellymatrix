cmake_minimum_required(VERSION 3.5)
project(JellyMatrix)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES main.cpp SafeArray.hpp SafeArray.hpp SafeMatrix.hpp)
add_executable(JellyMatrix ${SOURCE_FILES})