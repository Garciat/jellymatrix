#include <iostream>

#include "SafeArray.hpp"
#include "SafeMatrix.hpp"

using namespace std;

int main() {
    SafeArray<int> sa1(10);

    for (auto&& item : sa1) {
        cout << item << endl;
    }

    cout << sa1 << endl;

    SafeMatrix<int> sm1(2, 2);
    sm1.at(0, 0) = 1;
    sm1.at(0, 1) = 1;
    sm1.at(1, 0) = 1;

    SafeMatrix<int> sm2 = sm1;

    for (int i = 0; i < 10; ++i) {
        cout << sm2.at(0, 1) << endl;
        sm2 = sm2 * sm1;
    }

    return 0;
}
