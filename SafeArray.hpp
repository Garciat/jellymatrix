#pragma once

#include <cstdint>
#include <cassert>
#include <algorithm>
#include <iostream>

template <typename T>
class SafeArray {
public:
    SafeArray() {
        lo = hi = 0;
        data = nullptr;
    }

    SafeArray(std::size_t len)
            : SafeArray(0, len)
    { }

    SafeArray(std::size_t lo, std::size_t hi)
    {
        assert(lo < hi);

        this->lo = lo;
        this->hi = hi;

        data = new T[length()]{};
    }

    SafeArray(const SafeArray& other)
            : SafeArray(other.lo, other.hi)
    {
        std::copy(other.begin(), other.end(), this->begin());
    }

    ~SafeArray() {
        if (data) {
            delete[] data;
            data = nullptr;
        }
    }

    T* begin() {
        return &data[to_data_index(lo)];
    }

    T* end() {
        return &data[to_data_index(hi - 1) + 1];
    }

    const T* begin() const {
        return &data[to_data_index(lo)];
    }

    const T* end() const {
        return &data[to_data_index(hi - 1) + 1];
    }

    std::size_t length() const {
        return hi - lo;
    }

    T& operator[](std::size_t ix) {
        return data[to_data_index(ix)];
    }

    const T& operator[](std::size_t ix) const {
        bounds_check(ix);
        return data[to_data_index(ix)];
    }

    void print(std::ostream& os) const {
        os << '[';

        for (auto&& x : *this) {
            os << x << ", ";
        }

        os << ']';
    }

    SafeArray& operator=(const SafeArray& other) {
        this->~SafeArray();
        new(this) SafeArray(other);
        return *this;
    }

private:
    std::size_t to_data_index(std::size_t ix) const {
        bounds_check(ix);
        return ix - lo;
    }

    void bounds_check(std::size_t ix) const {
        assert(lo <= ix && ix < hi);
    }

    std::size_t lo, hi;
    T *data;
};


template <typename T>
std::ostream& operator<<(std::ostream& os, const SafeArray<T>& arr) {
    arr.print(os);
    return os;
}

