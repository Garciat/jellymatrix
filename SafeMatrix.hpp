#pragma once

#include "SafeArray.hpp"

template <typename T>
class SafeMatrix {
public:
    SafeMatrix(std::size_t row, std::size_t col)
            : SafeMatrix(0, row, 0, col)
    { }

    SafeMatrix(std::size_t row_lo, std::size_t row_hi, std::size_t col_lo, std::size_t col_hi)
            : row_lo(row_lo),
              row_hi(row_hi),
              col_lo(col_lo),
              col_hi(col_hi),
              data(row_lo, row_hi)
    {
        for (std::size_t i = row_lo; i < row_hi; ++i) {
            data[i] = SafeArray<T>(col_lo, col_hi);
        }
    }

    SafeMatrix(const SafeMatrix& that)
            : row_lo(that.row_lo),
              row_hi(that.row_hi),
              col_lo(that.col_lo),
              col_hi(that.col_hi),
              data(that.data)
    { }

    std::size_t count_rows() const {
        return row_hi - row_lo;
    }

    std::size_t count_cols() const {
        return col_hi - col_lo;
    }

    T& at(std::size_t i, std::size_t j) {
        return data[i][j];
    }

    const T& at(std::size_t i, std::size_t j) const {
        return data[i][j];
    }

    SafeMatrix add(const SafeMatrix& that) const {
        assert(this->row_lo == that.row_lo);
        assert(this->row_hi == that.row_hi);
        assert(this->col_lo == that.col_lo);
        assert(this->col_hi == that.col_hi);

        SafeMatrix<T> ret(row_lo, row_hi, col_lo, col_hi);

        for (std::size_t i = ret.row_lo; i < ret.row_hi; ++i) {
            for (std::size_t j = ret.col_lo; j < ret.col_hi; ++j) {
                ret.at(i, j) += this->at(i, j) + that.at(i, j);
            }
        }

        return ret;
    }

    SafeMatrix mul(const SafeMatrix& that) const {
        assert(this->row_lo == 0);
        assert(this->col_lo == 0);
        assert(this->row_lo == that.row_lo);
        assert(this->row_hi == that.row_hi);
        assert(this->col_lo == that.col_lo);
        assert(this->col_hi == that.col_hi);

        SafeMatrix<T> ret(this->row_hi, that.col_hi);

        for (std::size_t i = 0; i < ret.row_hi; ++i) {
            for (std::size_t j = 0; j < ret.col_hi; ++j) {
                for (std::size_t k = 0; k < this->col_hi; ++k) {
                    ret.at(i, j) += this->at(i, k) * that.at(k, j);
                }
            }
        }

        return ret;
    }

    void print(std::ostream& os) const {
        for (std::size_t i = row_lo; i < row_hi; ++i) {
            os << "[";
            for (std::size_t j = col_lo; j < col_hi; ++j) {
                os << at(i, j) << ' ';
            }
            os << "]\n";
        }
    }

    SafeMatrix& operator=(const SafeMatrix& that) {
        this->~SafeMatrix();
        new(this) SafeMatrix(that);
        return *this;
    }

private:
    std::size_t row_lo, row_hi, col_lo, col_hi;
    SafeArray<SafeArray<T>> data;
};


template <typename T>
std::ostream& operator<<(std::ostream& os, const SafeMatrix<T>& mat) {
    mat.print(os);
    return os;
}


template <typename T>
SafeMatrix<T> operator+(const SafeMatrix<T>& lhs, const SafeMatrix<T>& rhs) {
    return lhs.add(rhs);
}


template <typename T>
SafeMatrix<T> operator*(const SafeMatrix<T>& lhs, const SafeMatrix<T>& rhs) {
    return lhs.mul(rhs);
}
